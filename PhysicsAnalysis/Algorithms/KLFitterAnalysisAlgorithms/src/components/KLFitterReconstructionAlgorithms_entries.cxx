/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

/// @author Oliver Majersky
/// @author Baptiste Ravina

#include <KLFitterAnalysisAlgorithms/RunKLFitterAlg.h>
#include <KLFitterAnalysisAlgorithms/KLFitterFinalizeOutputAlg.h>

DECLARE_COMPONENT (EventReco::KLFitterFinalizeOutputAlg)
DECLARE_COMPONENT (EventReco::RunKLFitterAlg)
